﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDo.Models
{
    using System.Data.Entity;
    using System.Net.Sockets;

    public class Table
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<Tasks> Tasks { get; set; }

        public int UserId { get; set; }

        public DateTime Created { get; set; }
    }


    public class Tasks
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public DateTime Created { get; set; }

        public int TableId { get; set; }

        public int UserId { get; set; }

    }

    public class User
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Password { get; set; }

        public List<Tasks> Tasks { get; set; }

        public List<Table> Tables { get; set; }
    }

    public class SqlDatabase : DbContext
    {
        public SqlDatabase() : base("SqlDatabase")
        {
            this.Configuration.LazyLoadingEnabled = false; // Don't loading elements which I dont need ( non dynamicall queries)
        }

        public DbSet<Table> Tables { get; set; }

        public DbSet<Tasks> Tasks { get; set; }

        public DbSet<User> Users { get; set; }
    }
}
