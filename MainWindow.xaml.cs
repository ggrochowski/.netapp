﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySql.Data;
using MySql.Data.MySqlClient;
using ToDo.Models;
using ToDo.Classes;
using System.Data.Entity;



namespace ToDo
{
  
public partial class MainWindow : Window
    { 
        public static Classes.Task NewTask = new Classes.Task();
        //public static DataBase Baza = new DataBase();
        public static ToDoColumn Object = new ToDoColumn();
        public static MainWindow main = new MainWindow();
        //EditWindow editWindow = new EditWindow();
        LoginWindow LoginWindow = new LoginWindow();
        public MainWindow()
        {
            InitializeComponent();
            


        }
        private void Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();

        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (TableText.Text != "")
            {
                //Baza.CreateUser();
                ListBox Column = Object.CreateColumn();
                TextBox TaskBox = NewTask.AddTaskBox();
                Button AddTaskButton = NewTask.AddTaskButton();
                TextBox ColumnName = Object.AddNewText();
                TextBox TaskLabel = NewTask.AddTask();
                
                

                Column.SetResourceReference(ListBox.StyleProperty, "ListBox");
                ColumnName.SetResourceReference(TextBox.StyleProperty, "TextTemplate");
                ColumnName.Text = this.TableText.Text;
                main.MainGrid.Children.Add(ColumnName);
                main.MainGrid.Children.Add(Column);
                main.MainGrid.Children.Add(TaskBox);
                main.MainGrid.Children.Add(AddTaskButton);
                TableText.Clear();
                using (var data = new SqlDatabase())
                {
                    //var i = LoginWindow.Login.Text;
                    // ERROR
                    var T = new Models.Table
                    {

                        Name = ColumnName.Text,
                        UserId = LoginWindow.ID,
                        Created = DateTime.UtcNow.ToLocalTime()


                        //Tasks = Models.Tasks -> How there I can call Tasks from table tasks and add to list ?
                    };
                    data.Tables.Add(T);
                    data.SaveChanges();
                }
                var Data = new SqlDatabase();
                /*var jozki = Data.Users.Where(u => u.Name == "Józek").ToList();*/
                /*var jozkizewszystkim = Data.Users
                                                    .Where(u => u.Name == "Józek")
                                                    .Include(u => u.Tables) // query Join
                                                    .Include(u => u.Tables.Select(t => t.Tasks))

                                                    .ToList(); */

                Column.MouseLeftButtonUp += delegate
                {
                    EditWindow editWindow = new EditWindow();
                    if (Column.SelectedItem != null)
                    {
                        Column.SetResourceReference(ListBox.StyleProperty, "Null");
                        string text = Column.SelectedItem.ToString();
                        editWindow.Show();
                        editWindow.ShowWindow(text, ColumnName.Text, Column);
                    }

                    else
                    {
                        Column.SetResourceReference(ListBox.StyleProperty, "ListBox");

                    }
                };
                AddTaskButton.Click += delegate
                {
                    TaskLabel.Text = TaskBox.Text;
                    Column.Items.Add(TaskLabel.Text);

                    if (Column.Name.Length == 7)
                    {
                        int aaa= (int)Char.GetNumericValue(Column.Name[6]);
                        using (var AddTaskDB = new SqlDatabase())
                        {
                            var result = AddTaskDB.Users.SingleOrDefault(u => u.Name == LoginWindow.Login.Text);
                            var Task = new Models.Tasks
                            {
                                Text = TaskLabel.Text,
                                UserId = LoginWindow.ID,
                                TableId = aaa,
                                Created = DateTime.UtcNow.ToLocalTime()
                        };
                            AddTaskDB.Tasks.Add(Task);
                            AddTaskDB.SaveChanges(); /// <ERROR
                        }

                    }
                    if (Column.Name.Length == 8)
                    {
                        int ID = (int)Char.GetNumericValue(Column.Name[6]);
                        int ID2 = (int)Char.GetNumericValue(Column.Name[7]);
                        int ALL = ID + ID2;
                        using (var AddTaskDB2 = new SqlDatabase())
                        {
                            var result = AddTaskDB2.Users.SingleOrDefault(u => u.Name == LoginWindow.Login.Text);
                            var Task2 = new Models.Tasks
                            {
                                Text = TaskLabel.Text,
                                UserId = LoginWindow.ID,
                                TableId = ALL,
                                Created = DateTime.UtcNow.ToLocalTime()

                        };
                            AddTaskDB2.Tasks.Add(Task2);
                            AddTaskDB2.SaveChanges(); //EROR
                        }
                    }
                    TaskBox.Clear();
                    return;
                };
                
            }
            else
            {
                this.TableText.Focus();
            }
        }
        public void closeWindow()
        {
            this.Close();
        }

        private void StatisticsButton_Click(object sender, RoutedEventArgs e)
        {
            StatisticWindow statisticWindow = new StatisticWindow();
            statisticWindow.Show();
        }
        private void Focus(object sender, RoutedEventArgs e)
        {
            if(TableText.IsFocused == true)
            {
                UserLabel.Visibility = Visibility.Hidden;
            }
            else
            {
                UserLabel.Visibility = Visibility.Visible;
            }
        }

        private void UserLabel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            UserLabel.Visibility = Visibility.Hidden;
            TableText.Focus();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Hidden;
            LoginWindow window = new LoginWindow();
            window.Show();
        }
    }
}
