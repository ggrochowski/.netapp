﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ToDo.Models;

namespace ToDo
{
    /// <summary>
    /// Interaction logic for Register.xaml
    /// </summary>
    public partial class Register : Window
    {
        LoginWindow LoginWindow = new LoginWindow();
        public Register()
        {
            InitializeComponent();
        }

        private void ClickRegister(object sender, MouseButtonEventArgs e)
        {
            SqlDatabase db = new SqlDatabase();
            
            var result = db.Users.FirstOrDefault(a => a.Name == LoginBox.Text);
            int lastID = db.Users.Max(a => a.Id);
                                        
            if (result == null)
            {
                if (PasswordBox.Password == PasswordAgainBox.Password)
                {
                    using (var data = new SqlDatabase())
                    {
                        //var i = LoginWindow.Login.Text;
                        // ERROR
                        var adduser = new Models.User
                        {

                            Id = lastID + 1,
                            Name = LoginBox.Text,
                            Password = PasswordBox.Password,


                            //Tasks = Models.Tasks -> How there I can call Tasks from table tasks and add to list ?
                        };
                        data.Users.Add(adduser);
                        data.SaveChanges();
                        MessageBox.Show("Your account was created");
                    }
                }
                else {
                    MessageBox.Show("Passwords is diffrent ! Please correct passwords");
                    }
                }
            else
            {
                MessageBox.Show("This name is already in system");
            }
            }

        private void ClickBack(object sender, MouseButtonEventArgs e)
        {
            this.Close();
            LoginWindow.Show();
        }

        private void LoginLabelClick(object sender, MouseButtonEventArgs e)
        {
            LOGINLabel.Visibility = Visibility.Hidden;
            LoginBox.Focus();
        }

        private void PasswordLabelClick(object sender, MouseButtonEventArgs e)
        {
            PasswordLabel.Visibility = Visibility.Hidden;
            PasswordBox.Focus();
        }

        private void PassAgainLabelClick(object sender, MouseButtonEventArgs e)
        {
            PassAgainLabel.Visibility = Visibility.Hidden;
            PasswordAgainBox.Focus();
        }
    }
    }

