﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ToDo.Classes
{
    public class ToDoColumn
    {
        public double marginColumn;
        private double marginText;
        public static int IdColumn;
        private int IdText;

        public ToDoColumn()
        {
            marginColumn = 0;
            marginText = 0;
            IdColumn = 1;
            IdText = 0;

        }

        public ListBox CreateColumn()
        {
            ListBox Column = new ListBox();
            Column.Name = "Column" + IdColumn;
            Column.Width = 174;
            Column.Height = 418;
            Column.VerticalAlignment = VerticalAlignment.Top;
            Column.HorizontalAlignment = HorizontalAlignment.Left;
            Column.Margin = new Thickness(marginColumn, 98, 0, 0);
            marginColumn = marginColumn + 200;
            IdColumn += 1;
            return Column;

        }
        public TextBox AddNewText()
        {
            TextBox TableName = new TextBox();
            TableName.Name = "TableName" + IdText;
            TableName.Width = 120;
            TableName.Height = 30;
            TableName.VerticalAlignment = VerticalAlignment.Top;
            TableName.HorizontalAlignment = HorizontalAlignment.Left;
            TableName.Margin = new Thickness(40+marginText, 68, 50, 0);
            IdText += 1;
            marginText += 200;
            return TableName;
        }
        public void UpdateColumn()
        {
            marginColumn = 0;
            marginText = 0;
            IdColumn = 1;
            IdText = 0;

        }
    }
}
