﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySql.Data;
using MySql.Data.MySqlClient;
using ToDo.Models;
using ToDo.Classes;
using System.Data.Entity;


namespace ToDo.Classes
{
    class DataBaseReader
    {
        SqlDatabase db = new SqlDatabase();
        
        public DataBaseReader()
        {

        }
        public void ReadColumn(Grid MainGrid, string login)
        {
            
            var result = db.Users.SingleOrDefault(u => u.Name == login);
            var Column_max = db.Users.Where(u => u.Id == result.Id)
                                .Select(u => u.Tables.Select(t => t.Id))
                                .ToList();

            var Column_nameDB = db.Users.Where(u => u.Id == result.Id)
                                .Select(u => u.Tables.Select(a => a.Name));

            
                                
            
            List<string> Names = new List<string>();
            List<string> NamesTask = new List<string>();
            List<int> Id = new List<int>();
            foreach (var List in Column_max)
            {
                foreach (int count in List)
                {
                    Id.Add(count);
                    
                }
            }
            foreach (var List in Column_nameDB)
            {
                foreach (string item in List)
                {
                    Names.Add(item);
                    
                }
            }
            int maxloop = Column_max.Max().LastOrDefault();
            if (result.Id != 1)
            {
                maxloop = Id.Count;
               
            }
            
            for (int i = 0; i < maxloop; i++)
            { 
                
                ListBox Column = MainWindow.Object.CreateColumn();
                TextBox TaskBox = MainWindow.NewTask.AddTaskBox();
                Button AddTaskButton = MainWindow.NewTask.AddTaskButton();
                TextBox TaskLabel = MainWindow.NewTask.AddTask();
                TextBox ColumnName = MainWindow.Object.AddNewText();
               
                
                Column.SetResourceReference(ListBox.StyleProperty, "ListBox");
                ColumnName.SetResourceReference(TextBox.StyleProperty, "TextTemplate");
                
               
                var Tasks = db.Tasks
                                    .Where(t => t.TableId == i + 1)
                                    .Where(t => t.UserId == result.Id)
                                    .Select(t => t.Text)
                                    .ToList();
                foreach (string items in Tasks)
                {
                    
                    NamesTask.Add(items);
                    for (int x=0; x < NamesTask.Count; x++)
                    {
                        Column.Items.Add(NamesTask[x]);
                    }

                    NamesTask.Clear();
                }

                MainWindow.main.MainGrid.Children.Add(Column);
                MainWindow.main.MainGrid.Children.Add(TaskBox);
                MainWindow.main.MainGrid.Children.Add(AddTaskButton);
                
                ColumnName.Text = Names[i];
                MainWindow.main.MainGrid.Children.Add(ColumnName);

                
                    Column.MouseLeftButtonUp += delegate
                    {
                        EditWindow editWindow = new EditWindow();
                        if (Column.SelectedItem != null)
                        {
                            if (EditWindow.first == true)
                            {
                                Column.SetResourceReference(ListBox.StyleProperty, "Null");
                                string text = Column.SelectedItem.ToString();
                                
                                editWindow.Show();
                                editWindow.ShowWindow(text, ColumnName.Text, Column);
                                
                            }
                            else
                            {
                                Column.SetResourceReference(ListBox.StyleProperty, "Null");
                                string text = Column.SelectedItem.ToString();
                                editWindow.Show();
                                editWindow.ShowWindow(text, ColumnName.Text, Column);
                            }                            
                        }
                        
                        else
                        {
                            Column.SetResourceReference(ListBox.StyleProperty, "ListBox");
                            
                        }
                    };
                
                AddTaskButton.Click += delegate
                {

                    TaskLabel.Text = TaskBox.Text;
                    Column.Items.Add(TaskLabel.Text);
                   
                    
                    if (Column.Name.Length == 7)
                    {
                        int aaa = (int)Char.GetNumericValue(Column.Name[6]);
                        using (var AddTaskDB = new SqlDatabase())
                        {

                            var Task = new Models.Tasks
                            {
                                Text = TaskLabel.Text,
                                UserId = result.Id,
                                TableId = aaa,
                                Created = DateTime.UtcNow.ToLocalTime()
                            };
                            AddTaskDB.Tasks.Add(Task);
                            AddTaskDB.SaveChanges(); /// <ERROR
                        }

                    }
                    if (Column.Name.Length == 8)
                    {
                        int ID = (int)Char.GetNumericValue(Column.Name[6]);
                        int ID2 = (int)Char.GetNumericValue(Column.Name[7]);
                        int ALL = ID + ID2;
                        using (var AddTaskDB2 = new SqlDatabase())
                        {
                            var Task2 = new Models.Tasks
                            {
                                Text = TaskLabel.Text,
                                UserId = result.Id,
                                TableId = ALL,
                                Created = DateTime.UtcNow.ToLocalTime()

                            };
                            AddTaskDB2.Tasks.Add(Task2);
                            AddTaskDB2.SaveChanges(); //EROR
                        }
                    }
                    TaskBox.Clear();
                    return;


                };

                 
                
                
            }

            
        }

        private void TaskLabel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            MessageBox.Show("klik");
        }

    }
}
