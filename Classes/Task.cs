﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ToDo.Models;
using ToDo.Classes;
using System.Data.Entity;

namespace ToDo.Classes
{
    public class Task
    {
        private int TaskBoxID = 0;
        private int TaskID = 0;
        private int ButtonID = 0;
        private int position, positionLabel = 540;
        private int left, leftbutton = 0;

        public Task()
        {
            TaskBoxID = 0;
            TaskID = 0;
            ButtonID = 0;
            position = 540;
            positionLabel = 540;
            left = 0;
            leftbutton = 0;
        }
        public TextBox AddTaskBox()
        {
            TextBox Task = new TextBox();
            Task.Name = "TaskBox" + TaskBoxID;
            Task.Width = 120;
            Task.Height = 30;
            Task.VerticalAlignment = VerticalAlignment.Top;
            Task.HorizontalAlignment = HorizontalAlignment.Left;
            Task.Margin = new Thickness(left, 520, 0, 0);
            left += 200;
            TaskID += 1;
            return Task;
        }
        public TextBox AddTask()
        {
            TextBox Tekst = new TextBox();
            Tekst.Name = "Task" + TaskID;
            Tekst.Width = 80;
            Tekst.Height = 30;
            Tekst.TextAlignment = TextAlignment.Center;
            Tekst.Margin = new Thickness(0, position, 0, 0);
            TaskID += 1;
            position += 31;
            return Tekst;

        }
        public Label AddTaskLabel()
        {
            Label Label = new Label();
            Label.Width = 80;
            Label.Height = 30;
            Label.Content = "Add your task";
            Label.Background = Brushes.LightGray;
            Label.Background.Opacity = 0.3;
            Label.Margin = new Thickness(0, positionLabel, 0, 0);
            positionLabel += 31;
            return Label;

        }
        public Button AddTaskButton()
        {
            Button TaskButton = new Button();
            TaskButton.Name = "Button" + ButtonID;
            TaskButton.Content = "+";
            TaskButton.Width = 40;
            TaskButton.Height = 30;
            TaskButton.VerticalAlignment = VerticalAlignment.Top;
            TaskButton.HorizontalAlignment = HorizontalAlignment.Left;
            TaskButton.Margin = new Thickness(101 + leftbutton, 520, 0, 0);
            leftbutton += 200;
            TaskID += 1;
            TaskButton.SetResourceReference(Button.StyleProperty, "Bar");
            return TaskButton;

        }
        public void Update()
        {
            TaskBoxID = 0;
            TaskID = 0;
            ButtonID = 0;
            position = 540;
            positionLabel = 540;
            left = 0;
            leftbutton = 0;
        }
    }
}
