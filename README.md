# .NetAPP 
#    IGLO

## Iglo will be a desktop app via "Task Manager"
<br> **I would like :** <br>
* Create a simple and designed GUI
* Create Functionality desktop APP where user can add task
* Create Login/Register System
* Create Statistics system (plots and others)
* Create App which using a database

## Technologies:

* [Visual Studio](https://www.visualstudio.com/) - The website of Visual Studio IDE
* [SQL Server](https://www.microsoft.com/pl-pl/sql-server/sql-server-2017) - The website of SQL Server
* [Entity Framweork](https://msdn.microsoft.com/pl-pl/library/ff714342.aspx) - The website of Entity Framework
* Other graphics programs: Gimp


## Problems to be carried out in the project

**[TRELLO](https://trello.com/b/i0pONI15/to-do) - My plan of the project**

 ## Authors
* **Gracjan Grochowski** - Student IT
