namespace ToDo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTableId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tables", "TableId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tables", "TableId");
        }
    }
}
