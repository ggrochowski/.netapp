namespace ToDo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DataAdd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tasks", "Created", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tasks", "Created");
        }
    }
}
