namespace ToDo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class aaa : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Tables", "TableId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tables", "TableId", c => c.Int(nullable: false));
        }
    }
}
