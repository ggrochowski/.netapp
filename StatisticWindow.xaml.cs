﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ToDo.Models;
using ToDo.Classes;
using System.Data.Entity;

namespace ToDo
{

    public partial class StatisticWindow : Window
    {
        SqlDatabase db = new SqlDatabase();
        int position = 40;
        
        public StatisticWindow()
        {
            var user = db.Users.Include(u => u.Tables)
                                   .Include(t => t.Tasks)
                                   .Single(x => x.Name == LoginWindow.LoginText);

            InitializeComponent();
            ShowUserAndNumbers(user);
            ShowData(user);
            getItems(user);
            ShowButton.Click += ClickShow;
            
            
            
        }

        private void ShowUserAndNumbers(User user)
        {
           
            AccountLabel.Content = user.Name;
            NumberTaskLabel.Content = user.Tasks.Count();
            NumberTablesLabel.Content = user.Tables.Count();
            
        }
        private void ShowData(User user)
        {
            
            DateTime LongestTimeTable = user.Tables.Select(t => t.Created).First();
            DateTime LongestTimeTask = user.Tasks.Select(t => t.Created).First();
            DateTime LiveDate = DateTime.UtcNow.ToLocalTime();
            TimeSpan TimeTable = LiveDate.Subtract(LongestTimeTable);
            TimeSpan TimeTask = LiveDate.Subtract(LongestTimeTask);
            string TimeTableContent = TimeTable.Days + "days " + TimeTable.Hours + "h " + TimeTable.Minutes + "min ";
            string TimeTaskContent = TimeTask.Days + "days " + TimeTask.Hours + "h " + TimeTask.Minutes + "min";
            TimeTaskLabel.Content = TimeTaskContent;
            TimeTableLabel.Content = TimeTableContent;

        }
        private void getItems(User user)
        {
            List<string> Names = new List<string>();
            Names = user.Tables.Select(t => t.Name).ToList();
            foreach(string name in Names)
            {
                ComboBox.Items.Add(name);
            }
        }
        private Label CreateTableText(string table)
        {
            
            Label TableName = new Label();
            TableName.Content = table;
            TableName.FontFamily = new FontFamily("Stencil");
            TableName.Foreground = Brushes.White;
            TableName.FontSize = 18;
            TableName.Width = 100;
            TableName.Margin = new Thickness(0, 0, 0, 0);
            return TableName;
            //TableName.FontStyle = 
            //string TableNameContent = ComboBox.SelectedItem.ToString();

            //var Names = user.Tables.Select(t => t.Name == TableNameContent).Single();

            // TableName.Content = ComboBox.SelectedItem.ToString();
        }
        private Label AddTask(string Task,TimeSpan TimeTask)
        {
            Label TaskName = new Label();
            TaskName.Content = "Name Task : " + Task +" Task Time: "+ TimeTask.Days + "days " + TimeTask.Hours + "h " + TimeTask.Minutes + "min "; ;
            TaskName.FontSize = 16;
            TaskName.FontFamily = new FontFamily("Stencil");
            TaskName.Foreground = Brushes.White;
            TaskName.Width = 500;
            TaskName.HorizontalAlignment = HorizontalAlignment.Left;
            TaskName.VerticalAlignment = VerticalAlignment.Top;
            TaskName.Margin = new Thickness(10, 45+position, 0, 0);
            position += 25;
            return TaskName;

        }
        

        private void ClickShow(object sender, RoutedEventArgs e)
        {
            TableTaskTime.Children.Clear();
            position = 0;
            DateTime LiveDate = DateTime.UtcNow.ToLocalTime();
            var user = db.Users.Include(u => u.Tables)
                                    .Include(t => t.Tasks)
                                    .Single(x => x.Name == LoginWindow.LoginText);
            var Name = user.Tables.Where(t => t.Name == ComboBox.SelectedItem.ToString()).SingleOrDefault();
            
            if (ComboBox.SelectedItem != null)
            {
               var Table = CreateTableText(Name.Name);
               TableTaskTime.Children.Add(Table);

                foreach (var name in Name.Tasks)
                {
                    string Task = name.Text;
                    DateTime TaskTime = name.Created;
                    TimeSpan TasksTimeSubstract = LiveDate.Subtract(TaskTime);
                    var TaskMethod = AddTask(Task, TasksTimeSubstract);
                    TableTaskTime.Children.Add(TaskMethod);

                }
                

            }
            else
            {
                MessageBox.Show("Selected Item is empty");
            }

        }

        private void Back_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
