﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ToDo.Models;

namespace ToDo
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public static string LoginText { get; set; }
        public static int ID { get; set; }
        bool first = true;
        SqlDatabase db = new SqlDatabase();
        public LoginWindow()
        {
            InitializeComponent();
            
        }

        private void LogInLabel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            checkUser(Login.Text, Password.Password);
        }

        private void UserLabel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            UserLabel.Visibility = Visibility.Hidden;
            Login.Focus();

        }
        public void checkUser(string login, string pass)
        {
            Classes.DataBaseReader dataBase = new Classes.DataBaseReader();
            
            var result = db.Users.FirstOrDefault(a => a.Name == login);
            ID = db.Users.Where(u => u.Name == Login.Text)
                                    .Select(u => u.Id)
                                    .SingleOrDefault();

            if (result != null)
            {

                if (result.Password == pass || result == null)
                {
                    if(first == true)
                    {
                        MessageBox.Show("Correct Password");
                        this.Close();
                        MainWindow.main.Show();
                        LoginText = Login.Text;
                        dataBase.ReadColumn(MainWindow.main.MainGrid, Login.Text);
                        first = false;
                    }
                    else
                    {
                        MessageBox.Show("Correct Password");
                        this.Close();
                        MainWindow.main.Show();
                        
                    }
                   
                }
                else
                {
                    MessageBox.Show("Wrong Password or no user name");
                }

            }

        }

        private void PasswordLabelClick(object sender, MouseButtonEventArgs e)
        {
            PasswordLabel.Visibility = Visibility.Hidden;
            Password.Focus();
        }

        private void RegisterClick(object sender, MouseButtonEventArgs e)

        {
            
            Register register = new Register();
            /*using (var AddUser = new SqlDatabase())
            {
                var User = new Models.User
                {
                    Id=1,
                    Name="admin",
                    Password="admin",
                };
                AddUser.Users.Add(User);
                AddUser.SaveChanges();
            }*/
            this.Close();
            register.Show();
        }

        private void PasswordLabelClickFocus(object sender, RoutedEventArgs e)
        {
            PasswordLabel.Visibility = Visibility.Hidden;
            Password.Focus();
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
