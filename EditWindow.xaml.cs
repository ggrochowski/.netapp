﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ToDo.Models;

namespace ToDo
{
    /// <summary>
    /// Interaction logic for EditWindow.xaml
    /// </summary>
    public partial class EditWindow : Window
    {
        public static bool first = true;
        SqlDatabase db = new SqlDatabase();
        
        public EditWindow()
        {
            InitializeComponent();
        }

        public void ShowWindow(string Task,string ColumnName, ListBox Column)
        {

            
            var result = db.Tasks.Where(t => t.Text == Task).First();
            var result3 = db.Tables.Where(t => t.Name == ColumnName).First();
            if(Task != "null" || ColumnName != "null")
            {
                DBTask.Text = result.Text;
                DBTable.Text = ColumnName;
                DBDate.Content = result3.Created.ToString();
                TaskDate.Content = result.Created.ToString();
                Update.Click += delegate
                {
                   
                    if (first == true) { 
                        if (DBTask.Text != result.Text || DBTable.Text != ColumnName)
                        {
                            result.Text = DBTask.Text;
                            result3.Name = DBTable.Text;
                        }
                        db.SaveChanges();
                        this.Close();
                        MessageBox.Show("Sucessfull !!");
                        MainWindow.main.MainGrid.Children.Clear();
                        MainWindow.main.MainGrid.Children.Add(MainWindow.main.AddButton);
                        MainWindow.main.MainGrid.Children.Add(MainWindow.main.TableText);
                        MainWindow.Object.UpdateColumn();
                        MainWindow.NewTask.Update();
                        Classes.DataBaseReader data = new Classes.DataBaseReader();
                        data.ReadColumn(MainWindow.main.MainGrid, LoginWindow.LoginText);
                        Column.SetResourceReference(ListBox.StyleProperty, "ListBox");
                        first = false;
                    }
                    else
                    {
                        
                        
                        if (DBTask.Text != result.Text || DBTable.Text != ColumnName)
                        {
                            result.Text = DBTask.Text;
                            result3.Name = DBTable.Text;
                        }
                        db.SaveChanges();
                        this.Close();
                        MessageBox.Show("Sucessfull !!");
                        MainWindow.main.MainGrid.Children.Clear();
                        MainWindow.main.MainGrid.Children.Add(MainWindow.main.AddButton);
                        MainWindow.main.MainGrid.Children.Add(MainWindow.main.TableText);
                        MainWindow.Object.UpdateColumn();
                        MainWindow.NewTask.Update();
                        Classes.DataBaseReader data = new Classes.DataBaseReader();
                        data.ReadColumn(MainWindow.main.MainGrid, LoginWindow.LoginText);
                        Column.SetResourceReference(ListBox.StyleProperty, "ListBox");
                    }
                };

                Delete.Click += delegate
                {
                    MainWindow main = new MainWindow();
                    if (RadioTask.IsChecked == true )
                    {
                        if((MessageBox.Show("Do you want delete Task ??", "", MessageBoxButton.YesNo) == MessageBoxResult.Yes))
                        {
                            db.Tasks.Remove(result);
                            db.SaveChanges();
                            this.Close();
                            MessageBox.Show("Sucessfull");
                            MainWindow.main.MainGrid.Children.Clear();
                            MainWindow.main.MainGrid.Children.Add(MainWindow.main.AddButton);
                            MainWindow.main.MainGrid.Children.Add(MainWindow.main.TableText);
                            MainWindow.Object.UpdateColumn();
                            MainWindow.NewTask.Update();
                            Classes.DataBaseReader data = new Classes.DataBaseReader();
                            data.ReadColumn(MainWindow.main.MainGrid, LoginWindow.LoginText);
                            Column.SetResourceReference(ListBox.StyleProperty, "ListBox");
                        }
                        
                    }
                    else
                    {

                    }
                   /* if(RadioTab.IsChecked == true)
                    {
                        if (MessageBox.Show("Do you want delete Table ??", "", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                        {
                            List<string> NamesTask = new List<string>();
                            var Tasks = db.Tasks
                                    .Where(t => t.TableId == result.TableId)
                                    .ToList()
                                    ;
                           //var task = db.Tasks.Where(t => t.TableId == result.TableId).ToList();
                            //var taskID = db.Tasks.Where(t => t.TableId == result.TableId).ToList();
                            var fromTablesID = db.Tables.Where(t => t.Id == result.TableId).ToList();
                            
                            db.Tasks.Remove(Tasks.FirstOrDefault());
                            db.Tables.Remove(fromTablesID.FirstOrDefault());
                            db.Tables.Remove(result3);
                            db.SaveChanges();
                           
                            this.Close();
                            MessageBox.Show("Sucessfull");
                            main = new MainWindow();
                            main.Show();
                            MainWindow.Object.UpdateColumn();
                            MainWindow.NewTask.Update();
                            Classes.DataBaseReader data = new Classes.DataBaseReader();
                            data.ReadColumn(main.MainGrid, LoginWindow.LoginText);
                            Column.SetResourceReference(ListBox.StyleProperty, "ListBox");
                        }
                    } */
                        
                    
                };
                

                
            }
            else
            {
                MessageBox.Show("Some of value is null");
            }
            
        }

        
    }
}
